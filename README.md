# @theoparis/docker-client

## What is it

This project generates and publishes an npm package containing:

-   a whatwg-fetch based client for the Docker REST API
-   TypeScript definitions for this client

The client and its type definitions are automatically generated from the Docker API swagger specification in https://github.com/moby/moby using https://github.com/swagger-api/swagger-codegen (specifically the `typescript-fetch` language target).

## Usage

### Install

`npm install --save @theoparis/docker-client`

### Import

```ts
import { ImageApi } from "@theoparis/docker-client";

// API instance is lightweight to construct and stateless.
// With no options, connects to localhost. Use type information to understand options.
const api = new ImageApi();

// List images
api.imageList().then((list) => console.log(list));
```

### Where the docs at?

There isn't much in the way of a user guide at the moment. You need to rely on:

-   Automatically generated [API docs](https://creepinson.gitlab.io/docker-client)
-   Docker's own [REST API documentation](https://docs.docker.com/engine/api), generated from the same spec
-   JSDoc and type annotations in the library

Please file an issue if you have trouble figuring out how things work.

## Gotchas

### Versioning

🚨 **This package does not follow semver!** 🚨

The major.minor version number for the package is simply the version number of the Swagger API spec that the client was generated from. All bugfixes in the codegen, breaking or otherwise, will be released under an incremented patch version under the appropriate Swagger spec version.

If you want to avoid being broken by updates, please explicitly specify the patch version in your `package.json` or make use of your `package-lock.json` or `yarn.lock` lockfile.

### Authentication

Most Docker instances with the remote API enabled will be using transport level security (TLS). For example, Docker hosts created with `docker-machine` use TLS by default. This means you need a special set of certificates to communicate with the REST API.

Fortunately, the generated client allows you to inject your own implementation of `fetch`. If you are using this library from Node, [`node-fetch`](https://www.npmjs.com/package/node-fetch) is a Fetch API implementation that accepts an [`https.Agent`](https://nodejs.org/api/https.html#https_class_https_agent), which will allow you to point to the relevant certs.

As an example, here is how you might connect to a Docker instance created with `docker-machine create --driver virtualbox foo`:

```ts
import * as { FetchAPI, ImageApi } from "@theoparis/docker-client";
import * as https from "https";
import * as fs from "fs";
import nodeFetch from "node-fetch";

const agent = new https.Agent({
  cert: fs.readFileSync("D:\\vms\\docker\\machines\\foo\\cert.pem"),
  ca: fs.readFileSync("D:\\vms\\docker\\machines\\foo\\ca.pem"),
  key: fs.readFileSync("D:\\vms\\docker\\machines\\foo\\key.pem")
});
const fetch: FetchAPI = (url, init) => nodeFetch(url, { ...init, agent });

const api = new ImageApi({ basePath: "https://1.2.3.4:2376" }, undefined, fetch);

api.imageList().then(list => console.log(list));
```

### Streaming responses

Many endpoints on the API return streaming responses, which represent the progress of some activity initiated by your request (e.g. pulling an image). In `@theoparis/docker-client`, the methods corresponding to these endpoints return a `Promise` for a Fetch API [`Response`](https://developer.mozilla.org/en-US/docs/Web/API/Response) object.

You may use the standard Fetch API methods for awaiting completion of the `Response` stream:

```ts
const api = new ImageApi(...);
api.imageCreate("node")
  .then(res => res.text())
  .then(output => console.log(output));
```

If you are interested in incrementally consuming the output (perhaps to report progress), you can use the `Response#body` property. This will be some representation of a stream, and will depend on which Fetch API implementation you're using.

On Node, with `node-fetch`, `Response#body` will be a Node [`ReadableStream`](https://nodejs.org/api/stream.html#stream_readable_streams). You can work with this however you prefer to usually work with Node streams: e.g. using `pipe` or the [rxjs-stream](https://www.npmjs.com/package/rxjs-stream) library.

In the browser (which is untested) you'll end up with a [`ReadableStream`](https://developer.mozilla.org/en-US/docs/Web/API/ReadableStream) conforming to the [WHATWG Streams spec](https://github.com/whatwg/streams), although again, this depends on the browser and/or fetch polyfill you're using.
